package com.gitlab.soshibby.issuereporter.gitlab;

import java.util.HashMap;
import java.util.Map;

public class IssueHistory {
    private Map<String, Integer> issues = new HashMap<>();

    public void add(String hash, Integer issueId) {
        issues.put(hash, issueId);
    }

    public boolean exist(String hash) {
        return issues.containsKey(hash);
    }

    public Integer getIssueId(String hash) {
        return issues.get(hash);
    }
}

package com.gitlab.soshibby.issuereporter.gitlab;

import org.springframework.util.Assert;

public class ConfigValidator {

    public static void validate(Config config) {
        Assert.notNull(config, "Configuration is null.");
        Assert.hasText(config.getAccessToken(), "Access token is null or empty in config.");
        Assert.hasText(config.getGitlabURL(), "Gitlab url is null or empty in config.");
        Assert.isTrue(config.getGitlabProjectId() != 0, "Gitlab project id is not defined in config.");
    }

}

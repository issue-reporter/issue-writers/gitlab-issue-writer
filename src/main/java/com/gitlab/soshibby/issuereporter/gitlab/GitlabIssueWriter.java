package com.gitlab.soshibby.issuereporter.gitlab;

import com.gitlab.soshibby.issuereporter.issuewriter.IssueWriter;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GitlabIssueWriter implements IssueWriter {

    private static final Logger log = LoggerFactory.getLogger(GitlabIssueWriter.class);
    private Config config;
    private GitLabApi gitlab;
    private IssueHistory history;

    @Override
    public void init(String configuration) {
        config = Config.from(configuration);
        ConfigValidator.validate(config);

        gitlab = new GitLabApi(config.getGitlabURL(), config.getAccessToken());
        history = new IssueHistory();
    }

    @Override
    public void writeIssue(String hash, String header, String body) {
        log.info("Creating issue for hash '" + hash + "' in Gitlab.");

        if (history.exist(hash)) {
            log.info("An issue with hash '" + hash + "' already exist, skipping...");
        } else {
            try {
                Issue issue = gitlab.getIssuesApi().createIssue(config.getGitlabProjectId(), header, body, null, null, null, "test", null, null, null, null);
                history.add(hash, issue.getIid());
                log.info("Issue created in Gitlab.");
            } catch (GitLabApiException e) {
                throw new RuntimeException("Failed to create new issue in Gitlab.", e);
            }
        }
    }

    @Override
    public void writeIncident(String hash, String body) {
        log.info("Creating incident for hash '" + hash + "' in Gitlab.");

        if (!history.exist(hash)) {
            throw new RuntimeException("Couldn't find issue id for hash '" + hash + "' in issue history.");
        }

        Integer issueId = history.getIssueId(hash);

        try {
            gitlab.getNotesApi().createIssueNote(config.getGitlabProjectId(), issueId, body);
            log.info("Created incident in Gitlab.");
        } catch (GitLabApiException e) {
            throw new RuntimeException("Failed to create incident in Gitlab.", e);
        }
    }
}
